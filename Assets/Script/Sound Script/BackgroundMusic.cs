using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundMusic : MonoBehaviour
{
    private static BackgroundMusic instance = null;
    private AudioSource audioSource;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (ShouldMuteMusicInScene(scene.name))
        {
            audioSource.Pause();
        }
        else
        {
            audioSource.UnPause();
        }
    }

    private bool ShouldMuteMusicInScene(string sceneName)
    {
        // �кت��� Scene ����ͧ��ûԴ���§�ŧ
        string[] scenesToMute = { "VDO1", "VDO2", "VDO3" };

        foreach (string muteScene in scenesToMute)
        {
            if (sceneName == muteScene)
            {
                return true;
            }
        }
        return false;
    }
}
