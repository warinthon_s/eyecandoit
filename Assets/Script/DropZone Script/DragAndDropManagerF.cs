using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragAndDropManagerF : MonoBehaviour
{
    public Button completionButton; // �������л�ҡ�������ҧ�ú�ء�ش
    public Button wrongDropButton; // �������л�ҡ�������ҧ�Դ�ش
    public int totalDropZones = 2; // �ӹǹ�ش����ͧ�ҧ�ѵ�ط�����
    private int correctDrops = 0; // �ӹǹ�ѵ�ط���ҧ㹨ش���١��ͧ
    private int wrongDrops = 0; // �ӹǹ�ѵ�ط���ҧ㹨ش���Դ

    private void Start()
    {
        // ��͹�����͹�������
        if (completionButton != null)
        {
            completionButton.gameObject.SetActive(false);
        }

        if (wrongDropButton != null)
        {
            wrongDropButton.gameObject.SetActive(false); // ��͹��������ҧ�Դ�͹�������
        }
    }

    public void IncrementCorrectDrops()
    {
        correctDrops++;
        CheckCompletion();
    }

    public void DecrementCorrectDrops()
    {
        correctDrops--;
        CheckCompletion();
    }

    public void IncrementWrongDrops()
    {
        wrongDrops++;
        CheckCompletion();
    }

    public void DecrementWrongDrops()
    {
        wrongDrops--;
        CheckCompletion();
    }

    private void CheckCompletion()
    {
        if (completionButton != null && wrongDropButton != null)
        {
            // �ʴ�����������ҧ�ú�ء�ش
            if (correctDrops == totalDropZones)
            {
                completionButton.gameObject.SetActive(true);
                wrongDropButton.gameObject.SetActive(false);
            }
            else if (wrongDrops > 0)
            {
                wrongDropButton.gameObject.SetActive(true);
                completionButton.gameObject.SetActive(false);
            }
            else
            {
                completionButton.gameObject.SetActive(false);
                wrongDropButton.gameObject.SetActive(false);
            }
        }
    }
}
