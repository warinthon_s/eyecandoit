using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAndDropF : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Transform[] correctDropZones; // �ش����ͧ����ҡ��ҧ
    public Transform[] wrongDropZones; // �ش����ҧ�Դ

    private Vector3 startPosition;
    private Transform originalParent;
    private DragAndDropManagerF manager;
    private bool isDroppedCorrect = false; // ��������͵�Ǩ�ͺ��Ҷ١�ҧ���������ѧ㹨ش���١��ͧ
    private bool isDroppedWrong = false; // ��������͵�Ǩ�ͺ��Ҷ١�ҧ���������ѧ㹨ش���Դ

    private void Start()
    {
        manager = FindObjectOfType<DragAndDropManagerF>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
        originalParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        transform.SetParent(transform.root);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        bool droppedInCorrectZone = false;
        bool droppedInWrongZone = false;

        foreach (Transform dropZone in correctDropZones)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(dropZone as RectTransform, Input.mousePosition))
            {
                transform.SetParent(dropZone);
                transform.position = dropZone.position;
                droppedInCorrectZone = true;
                if (!isDroppedCorrect)
                {
                    manager.IncrementCorrectDrops(); // �����ӹǹ�ѵ�ط��١�ҧ㹨ش���١��ͧ
                    isDroppedCorrect = true;
                }
                if (isDroppedWrong)
                {
                    manager.DecrementWrongDrops(); // Ŵ�ӹǹ�ѵ�ط���ҧ�Դ
                    isDroppedWrong = false;
                }
                break;
            }
        }

        if (!droppedInCorrectZone)
        {
            foreach (Transform wrongDropZone in wrongDropZones)
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(wrongDropZone as RectTransform, Input.mousePosition))
                {
                    transform.SetParent(wrongDropZone);
                    transform.position = wrongDropZone.position;
                    droppedInWrongZone = true;
                    if (!isDroppedWrong)
                    {
                        manager.IncrementWrongDrops(); // �����ӹǹ�ѵ�ط���ҧ�Դ
                        isDroppedWrong = true;
                    }
                    if (isDroppedCorrect)
                    {
                        manager.DecrementCorrectDrops(); // Ŵ�ӹǹ�ѵ�ط���ҧ㹨ش���١��ͧ
                        isDroppedCorrect = false;
                    }
                    break;
                }
            }

            if (!droppedInWrongZone)
            {
                transform.SetParent(originalParent);
                transform.position = startPosition;
                if (isDroppedCorrect)
                {
                    manager.DecrementCorrectDrops(); // Ŵ�ӹǹ�ѵ�ط��١�ҧ㹨ش���١��ͧ
                    isDroppedCorrect = false;
                }
                if (isDroppedWrong)
                {
                    manager.DecrementWrongDrops(); // Ŵ�ӹǹ�ѵ�ط���ҧ�Դ
                    isDroppedWrong = false;
                }
            }
        }
    }
}