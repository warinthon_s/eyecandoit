using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameAppFlowManager : MonoBehaviour
{
    protected static bool isSceneOptionLoaded;

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName,LoadSceneMode.Single);
    }

    public void UnloadScene(string sceneName)
    {
        SceneManager.UnloadSceneAsync(sceneName);
    }

    public void LoadSceneAdditive(string sceneName)
    {
        SceneManager.LoadScene(sceneName,LoadSceneMode.Additive);
    }

    public void LoadOptionsScene(string optionSceneName)
    {
        if (!isSceneOptionLoaded)
        {
            SceneManager.LoadScene(optionSceneName ,LoadSceneMode.Additive);
            isSceneOptionLoaded = true;
        }
    }
    public void UnloadOptionsScene(string optionSceneName)
    {
    if (isSceneOptionLoaded)
    {
        SceneManager.UnloadSceneAsync(optionSceneName);
        isSceneOptionLoaded = false;
    }
    }

    public void RestartCurrentScene()
    {
        // Get the current active scene
        Scene currentScene = SceneManager.GetActiveScene();
        Debug.Log("Restarting scene: " + currentScene.name);

        // Load the current scene again
        SceneManager.LoadScene(currentScene.name);
    }

    public void ExitGame()
{
    Application.Quit();
}

#region Scene Load and Unload Events Handler
private void OnEnable()
{
    SceneManager.sceneUnloaded += SceneUnloadEventHandler;
    SceneManager.sceneLoaded += SceneLoadedEventHandler;
}

private void OnDisable()
{
    SceneManager.sceneUnloaded -= SceneUnloadEventHandler;
    SceneManager.sceneLoaded -= SceneLoadedEventHandler;
}

private void SceneUnloadEventHandler(Scene scene)
{

}
private void SceneLoadedEventHandler(Scene scene, LoadSceneMode mode)
{
    if (scene.name.CompareTo("SceneOptions") != 0)
    {
        isSceneOptionLoaded = false;
    }
}
#endregion
}