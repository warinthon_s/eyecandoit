using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public GameObject PanelPauseMenu;
    public static bool GameStop = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameStop)
            {
                Resume();
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Pause();
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }

    public void Resume()
    {
        PanelPauseMenu.SetActive(false);
        Time.timeScale = 1f;//เล่นเกม
        GameStop = false;
    }

    void Pause()
    {
        PanelPauseMenu.SetActive(true);
        Time.timeScale = 0f;//หยุดเกม
        GameStop = true;
    }
}
