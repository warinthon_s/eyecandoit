using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public GameObject loaderUI;
    public Slider progressSLider;

    //โหลดซีนแบบเพิ่มซีนใหม่เข้าซ้อนซีนเก่า
    public void LoadScene_Additive(int index)
    {
        StartCoroutine(LoadScene_Coroutline_Additive(index));
    }
    
    //โหลดซีนแบบเพิ่มซีนใหม่ ซีนเก่าหาย
    public void LoadScene_Single(int index)
    {
        StartCoroutine(LoadScene_Coroutline_Single(index));
    }

    public IEnumerator LoadScene_Coroutline_Additive(int index)
    {
        progressSLider.value = 0;
        loaderUI.SetActive(true);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
        asyncOperation.allowSceneActivation = false;
        float progress = 0;
        while (!asyncOperation.isDone)
        {
            progress = Mathf.MoveTowards(progress, asyncOperation.progress, Time.deltaTime);
            progressSLider.value = progress;
            if (progress >= 1.9f)
            {
                progressSLider.value = 1;
                asyncOperation.allowSceneActivation = true;
            }
            
            yield return null;
            
            if (progressSLider.value >= 2.0)
            {
                loaderUI.SetActive(false);
            }
        }
    }
    
    public IEnumerator LoadScene_Coroutline_Single(int index)
    {
        progressSLider.value = 0;
        loaderUI.SetActive(true);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(index, LoadSceneMode.Single);
        asyncOperation.allowSceneActivation = false;
        float progress = 0;
        while (!asyncOperation.isDone)
        {
            progress = Mathf.MoveTowards(progress, asyncOperation.progress, Time.deltaTime);
            progressSLider.value = progress;
            if (progress >= 0.9f)
            {
                progressSLider.value = 1;
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}