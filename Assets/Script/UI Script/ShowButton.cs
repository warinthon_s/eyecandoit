using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ShowButton : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        button.gameObject.SetActive(false);
        videoPlayer.loopPointReached += OnVideoEnd;

    }

    void OnVideoEnd(VideoPlayer vp)
    {
        button.gameObject.SetActive(true);
    }
}

