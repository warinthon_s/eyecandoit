using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindMotion : MonoBehaviour
{
    public RectTransform uiElement;
    public float amplitude = 5f; // �����٧�ͧ����
    public float frequency = 1f; // �������ͧ����

    private Vector2 originalPosition;

    void Start()
    {
        if (uiElement == null)
        {
            uiElement = GetComponent<RectTransform>();
        }

        originalPosition = uiElement.anchoredPosition;
    }

    void Update()
    {
        float newY = originalPosition.y + Mathf.Sin(Time.time * frequency) * amplitude;
        uiElement.anchoredPosition = new Vector2(originalPosition.x, newY);
    }
}
