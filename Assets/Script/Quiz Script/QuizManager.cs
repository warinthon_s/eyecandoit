using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizManager : MonoBehaviour
{
    public Button[] answerButtons; // �����ӵͺ������
    public TextMeshProUGUI scoreText; // TextMeshProUGUI ����Ѻ�ʴ���ṹ
    public GameObject endGameUI; // UI ����Ѻ�ʴ�����ͨ���
    private int score = 0; // ������纤�ṹ
    private int totalQuestions;

    void Start()
    {
        totalQuestions = answerButtons.Length; // �ӹǹ�Ӷ��������
        endGameUI.SetActive(false); // ��͹ UI ����㹵͹�á
        foreach (Button button in answerButtons)
        {
            button.onClick.AddListener(() => OnAnswerSelected(button));
        }
    }

    void OnAnswerSelected(Button button)
    {
        // ��Ǩ�ͺ��Ҥӵͺ�١��ͧ�������
        bool isCorrect = button.GetComponent<AnswerButton>().isCorrect;

        if (isCorrect)
        {
            score++;
        }

        // �Դ�����ҹ���������������ԡ���
        button.interactable = false;

        // �Ѿഷ��ṹ����ʴ�
        scoreText.text = "�������" ;
        scoreText.text = "��ṹ�����: " + score;

        // ��Ǩ�ͺ��ҷӤӶ���ú�����ѧ
        CheckEndGame();
    }

    void CheckEndGame()
    {
        bool allAnswered = true;
        foreach (Button button in answerButtons)
        {
            if (button.interactable)
            {
                allAnswered = false;
                break;
            }
        }

        if (allAnswered)
        {
            endGameUI.SetActive(true);
        }
    }
}
